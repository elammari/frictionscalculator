﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace FrictionsCalculator
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        
            
        public MainWindow()
        {
            InitializeComponent();

            // getResults();
            CalculateBtn.Click += CalculateBtn_Click;

            CleanButton.Click += CleanButton_Click;

        }

        public void getResults()
        {

            try
            {

                int firstNumeratorValue = Int32.Parse(firstNumerator.Text);
                int firstDenominatorValue = Int32.Parse(firstDenominator.Text);
                int secondNumeratorValue = Int32.Parse(secondNumerator.Text);
                int secondDenominatorValue = Int32.Parse(secondDenominator.Text);
                string operations = comboBox.Text;

                // Show results in Numerator/ Denominator format
                // For Addition 
                if (operations == "+")
                {

                    resultNumerator.Text = (new FrictionCalculator.Fraction(firstNumeratorValue, firstDenominatorValue) + new FrictionCalculator.Fraction(secondNumeratorValue, secondDenominatorValue)).Numerator.ToString();
                    resultDenominator.Text = (new FrictionCalculator.Fraction(firstNumeratorValue, firstDenominatorValue) + new FrictionCalculator.Fraction(secondNumeratorValue, secondDenominatorValue)).Denominator.ToString();

                    // For Subtraction 
                }
                else if (operations == "-")
                {

                    resultNumerator.Text = (new FrictionCalculator.Fraction(firstNumeratorValue, firstDenominatorValue) - new FrictionCalculator.Fraction(secondNumeratorValue, secondDenominatorValue)).Numerator.ToString();
                    resultDenominator.Text = (new FrictionCalculator.Fraction(firstNumeratorValue, firstDenominatorValue) - new FrictionCalculator.Fraction(secondNumeratorValue, secondDenominatorValue)).Denominator.ToString();

                    // For Multiplication
                }
                else if (operations == "*")
                {

                    resultNumerator.Text = (new FrictionCalculator.Fraction(firstNumeratorValue, firstDenominatorValue) * new FrictionCalculator.Fraction(secondNumeratorValue, secondDenominatorValue)).Numerator.ToString();
                    resultDenominator.Text = (new FrictionCalculator.Fraction(firstNumeratorValue, firstDenominatorValue) * new FrictionCalculator.Fraction(secondNumeratorValue, secondDenominatorValue)).Denominator.ToString();

                    // For Division
                }
                else if (operations == "/")
                {

                    resultNumerator.Text = (new FrictionCalculator.Fraction(firstNumeratorValue, firstDenominatorValue) / new FrictionCalculator.Fraction(secondNumeratorValue, secondDenominatorValue)).Numerator.ToString();
                    resultDenominator.Text = (new FrictionCalculator.Fraction(firstNumeratorValue, firstDenominatorValue) / new FrictionCalculator.Fraction(secondNumeratorValue, secondDenominatorValue)).Denominator.ToString();

                }
            }
            catch (Exception ex) {
                MessageBox.Show(ex.Message);
                
            }
        }

        public void ClearResults()
        {


            firstNumerator.Text = "";
            firstDenominator.Text = "";
            secondNumerator.Text = "";
            secondDenominator.Text = "";
            resultNumerator.Text = "";
            resultDenominator.Text = "";

        }



        void CleanButton_Click(object sender, RoutedEventArgs e)
        {
            // MessageBox.Show("Message here");
            ClearResults();

        }
        void CalculateBtn_Click(object sender, RoutedEventArgs e)
        {
            // MessageBox.Show("Message here");
            getResults();


        }
    }
}
